import classes from './App.module.scss';
import ToDoList from "./Component/ToDoList/ToDoList";

function App() {
    return (
        <div className={classes.App}>
            <h1 className={classes.Title}>What's the Plan for Today?</h1>
            <ToDoList/>
        </div>
    );
}

export default App;
