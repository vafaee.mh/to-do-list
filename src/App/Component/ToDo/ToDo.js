import classes from "./ToDo.module.scss";
import {useState} from "react";
import {RiCloseCircleLine, TiEdit} from "react-icons/all";
import ToDoForm from "../ToDoForm/ToDoForm";
import classnames from 'classnames'
export default function ToDo({todos, completeTodo, removeTodo, updateTodo}) {
    const [edit, setEdit] = useState({
        id: null,
        value: ''
    })
    const onsubmitUpdate = value => {
        updateTodo(edit.id, value)
        setEdit({
            id: null,
            value: ''
        })
    }
    if (edit.id) {
        return (<ToDoForm edit={edit} onSubmit={onsubmitUpdate}/>)
    }
    return todos.map((todo, index) => (
        <div key={index} className={`${(todo.isComplete) ? classnames(classes.todoRow ,classes.complete) : classes.todoRow}`}>
            <div key={todo.id} onClick={() => completeTodo(todo.id)}>{todo.text}</div>
            <div className={classes.Icon}>
                <RiCloseCircleLine
                    onClick={() => removeTodo(todo.id)}
                    className={classes.RemoveIcon}
                />
                <TiEdit
                    onClick={() => setEdit({id: todo.id, value: todo.text})}
                    className={classes.EditIcon}
                />
            </div>
        </div>
    ))
}