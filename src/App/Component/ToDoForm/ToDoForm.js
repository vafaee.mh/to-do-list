import classes from "./ToDoForm.module.scss";
import {useState, useEffect, useRef} from "react";

export default function ToDoForm({onSubmit, edit}) {
    const [input, setInput] = useState(edit ? edit.value : '');
    const inputRef = useRef(null);
    useEffect(() => {
        inputRef.current.focus();
    }, [])
    const handleSubmit = (e) => {
        e.preventDefault()
        onSubmit({
            id: Math.floor(Math.random() * 1000),
            text: input
        })
        setInput('')

    }
    return (

        <div className={classes.ToDoForm}>
            <form className={classes.Form} onSubmit={handleSubmit}>
                {
                    edit ?
                        <>
                            <input type="text"
                                   value={input}
                                   onChange={(e) => setInput(e.target.value)}
                                   className={classes.Input}
                                   ref={inputRef}
                                   placeholder={'updateTodo'}
                            />
                            <button className={classes.ButtonEdit} type={"submit"}>updateTodo</button>
                        </>
                        :
                        <>
                            <input type="text"
                                   value={input}
                                   onChange={(e) => setInput(e.target.value)}
                                   className={classes.Input}
                                   ref={inputRef}
                                   placeholder={'Add todo'}
                            />
                            <button className={classes.Button} type={"submit"}>AddTodo</button>
                        </>
                }

            </form>
        </div>
    );
}