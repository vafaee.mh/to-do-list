import classes from "./ToDoList.module.scss";
import ToDoForm from "../ToDoForm/ToDoForm";
import {useState} from "react";
import ToDo from "../ToDo/ToDo";

export default function ToDoList() {
    const [todos, setTodos] = useState([])

    const addToDO = todo => {
        if (!todo.text || /^\s*$/.test(todo.text)) {
            return;
        }
        const newTodos = [todo, ...todos]
        setTodos(newTodos)
    }

    const updateTodo = (todoId, newValue) => {
        if (!newValue.text || /^\s*$/.test(newValue.text)) {
            return;
        }
        setTodos(prev => prev.map(item => item.id === todoId ? newValue : item))
    }

    const completeTodo = id => {
        const updatedTodos = todos.map(todo => {
            if (todo.id === id) {
                todo.isComplete = !todo.isComplete
            }
            return todo
        })
        setTodos(updatedTodos)
    }

    const removeTodo = id => {
        const removeArr = [...todos].filter(todo => todo.id !== id)
        setTodos(removeArr)
    }

    return (
        <div className={classes.ToDoList}>
            <ToDoForm onSubmit={addToDO}/>
            <ToDo todos={todos} completeTodo={completeTodo} removeTodo={removeTodo} updateTodo={updateTodo}/>
        </div>
    );
}